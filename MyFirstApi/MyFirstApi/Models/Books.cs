﻿namespace MyFirstApi.Models
{
    public class Books
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public int YearPublished { get; set; }
        public double Price { get; set; } 
    }
}
