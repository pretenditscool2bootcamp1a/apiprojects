﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BooksAPI.Models;

public partial class Book
{
    
    public int BookId { get; set; }

    
    
    public string? Title { get; set; } 

    
    public string? Author { get; set; } 

    public string? Publisher { get; set; }

    
    
    public int YearPublished { get; set; }

    public double Price { get; set; }
}
