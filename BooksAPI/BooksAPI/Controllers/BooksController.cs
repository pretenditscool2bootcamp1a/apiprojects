﻿using BooksAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;


namespace BooksAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BooksController : Controller
    {
        public static string connStr = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BooksDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public static SqlConnection cn = new SqlConnection(connStr);

        private static List<Book> _books = new List<Book>();
        /*private readonly List<Book> _Books =
            new List<Book>
            {
            // Seeding data that would really live in
            // a database somewhere
                new Book { BookId = 1,
                    Title = "The Prefect",
                    Author = "Alistair Reynolds",
                    Publisher = "Random House",
                    YearPublished = 2013,
                    Price = 15.75
                },
                new Book { BookId = 2,
                    Title = "Dream of 1000 nights",
                    Author = "Alistair Reynolds",
                    Publisher = "Tor Publishing",
                    YearPublished = 2011,
                    Price = 13.25
                },
                new Book { BookId = 3,
                    Title = "Leviathan Wakes",
                    Author = "James S.A. Corey",
                    Publisher = "Wow Publishing",
                    YearPublished = 2009,
                    Price = 17.59
                },
                new Book { BookId = 4,
                    Title = "A Method For Madness",
                    Author = "David Gerrold",
                    Publisher = "Tor Publishing",
                    YearPublished = 2024,
                    Price = 25.33
                },
                new Book { BookId = 5,
                    Title = "The Unbearable Lightness of Being",
                    Author = "Milan Kundera",
                    Publisher = "Harper and Row",
                    YearPublished = 1984,
                    Price = 20.15
                },
                new Book { BookId = 6,
                    Title = "The Book of Laugher and Forgetting",
                    Author = "Milan Kundera",
                    Publisher = "Harper and Row",
                    YearPublished = 1979,
                    Price = 15.43
                },
            };*/

        // get api/books (return all books)
        [HttpGet]
        public IEnumerable<Book> Get()
        {

            cn.Open();
            string sql = "Select *  From books";
            SqlCommand cmd = new SqlCommand(sql, cn);

            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Book book = new Book();
                book.BookId = (int)dr["BookId"];
                book.Title = (string)dr["Title"];
                book.Author = (string)dr["Author"];
                book.Publisher = (string)dr["Publisher"];
                book.YearPublished = (int)dr["YearPublished"];
                book.Price = (double)dr["Price"];
                _books.Add(book);
            }
            dr.Close();
            cn.Close();
            return _books;

        }

        // get api/books/5 return book where bookid = 5
        [HttpGet("{BookId}")]
        public ActionResult<Book> Get(int BookId)
        {

            cn.Open();
            string sql = "Select *  From books";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlParameter sqlParameter = new SqlParameter("BookId", BookId);

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    Book book = new Book();
                    book.BookId = (int)dr["BookId"];
                    book.Title = (string)dr["Title"];
                    book.Author = (string)dr["Author"];
                    book.Publisher = (string)dr["Publisher"];
                    book.YearPublished = (int)dr["YearPublished"];
                    book.Price = (double)dr["Price"];
                    _books.Add(book);
                }

                Book book2 = _books.FirstOrDefault(x => x.BookId == BookId);
                if (book2 == null)
                {
                    dr.Close();
                    cn.Close();
                    return NotFound();
                }
                dr.Close();
                cn.Close();
                return Ok(book2);
            }
            cn.Close();
        }


        [HttpPost]
        public ActionResult Post(Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            /*book.BookId = _books.Count + 1;*/
            cn.Open();
            string sql = "INSERT INTO books (BookId, Title, Author, Publisher, YearPublished, Price) " +
                           "VALUES (@BookId, @Title, @Author, @Publisher, @YearPublished, @Price)";
            //SqlCommand cmd = new SqlCommand(sql, cn);
            using (SqlConnection connection = new SqlConnection(connStr))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                int tempid = book.BookId;
                command.Parameters.AddWithValue("@BookId", book.BookId);
                command.Parameters.AddWithValue("@Title", book.Title);
                command.Parameters.AddWithValue("@Author", book.Author);
                command.Parameters.AddWithValue("@Publisher", book.Publisher);
                command.Parameters.AddWithValue("@YearPublished", book.YearPublished);
                command.Parameters.AddWithValue("@Price", book.Price);
                
                try
                {
                    connection.Open();
                    int rowsAffected = command.ExecuteNonQuery();
                    Console.WriteLine($"{rowsAffected} Books added");
                    cn.Close();
                    if (rowsAffected == 0)
                    {
                        return BadRequest("Invalid data, no rows created");
                    }
                    string location = $"api/books/{book.BookId}";
                    return Created(location, book);
                }
                catch (Exception ex)
                {
                    cn.Close();
                    return BadRequest(ex.Message);

                }
            }
            // int rowsChanged = cmd.ExecuteNonQuery();



            
        }

        [HttpPut("{id}")]
        public ActionResult Put(Book book, int id)
        {
            var context = new BooksDbContext();
            Book dbBook = context.Books.Find(id);
            if (dbBook == null)
            {
                return NotFound();
            }
/*            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }*/
            else 
            {
                if (book.Title != null)
                {
                    dbBook.Title = book.Title;
                }
                if (book.Author != null)
                {
                    dbBook.Author = book.Author;
                }
                if (book.Publisher != null)
                {
                    dbBook.Publisher = book.Publisher;
                }
                if (book.YearPublished != null && book.YearPublished != 0)
                {
                    dbBook.YearPublished = book.YearPublished;
                }
                if (book.Price != null && book.Price != 0)
                {
                    dbBook.Price = book.Price;
                }
                context.SaveChanges();
                return Ok();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var context = new BooksDbContext();
            Book dbBook = context.Books.Find(id);
            if (dbBook == null)
            {
                return NotFound();
            }
            else
            {
                context.Books.Remove(dbBook);
                context.SaveChanges();
                return Ok();
            }
        }
    }
}
